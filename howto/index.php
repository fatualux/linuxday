<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');

if (conf('is_physical') == false) {
	header('Location: /');
	exit();
}

lugheader ('Linux Day ' . conf('current_year') . ': Organizzati');

?>

<h2>Organizzati</h2>

<p>
	Nella tua zona nessuno organizza un Linux Day? E allora, fallo tu!
</p>

<p>
	Per organizzare un Linux Day non è necessario essere un LUG, una associazione o altro: basta qualche amico, un pò di buona volontà, e seguire alcuni semplici consigli e raccomandazioni.
</p>

<ul>
	<li>
		accertatevi di aver letto le <a href="<?php echo makeurl('/lineeguida') ?>">Linee Guida</a>. Queste sono le uniche regole che occorre seguire per aderire al Linux Day: la data è quella di <?php echo strtolower(conf('human_date')) ?>, l'accesso del pubblico deve essere gratuito, e tra i temi trattati non devono apparire applicazioni proprietarie (neanche se girano su Linux!) o comunque argomenti opposti allo spirito di condivisione e apertura della community che intendete rappresentare
	</li>
	<li>
		trovate <strong>una sede</strong>. Questa è probabilmente la parte più complessa dell'organizzazione, in quanto per allestire qualsiasi attività servono almeno uno o due locali e non sempre si trovano gratuitamente o a prezzo accessibile. Sicuramente si può iniziare chiedendo nelle scuole e nelle sedi universitarie, o valutare qualche sala del Comune, ma sono anche apprezzate soluzioni creative come gazebo o aree coperte nelle piazze (occhio a chiedere l'autorizzazione di occupazione suolo pubblico in Comune!), locali pubblici (nessuno vieta di "occupare" amichevolmente la birreria o il ristorante di un amico, purché come detto sopra non ci siano obblighi di spesa per il pubblico), aree all'interno dei centri commerciali (previa ovvia autorizzazione)... I limiti sono la fantasia e l'audacia!
	</li>
	<li>
		definite dei <strong>contenuti</strong>. Molto dipende da quanti e quali spazi avete trovate nella fase precedente (una sala? Due? Sei?) e da quante persone sono coinvolte nel vostro gruppo. Il format più comune è quello dei talk, percui sono necessari relatori che sappiano a turno illustrare i vari temi scelti ("Cos'é il Software Libero", "Panoramica su Linux", "Funzioni Avanzate di LibreOffice", o quel che volete) nonché la disponibilità di sedie e magari uno o più proiettori per le slides. Ma non è l'unica modalità: c'è chi, avendo disponibilità di qualche computer, allestisce banchetti tematici ed interattivi presso cui è possibile circolare e soffermarsi per maggiori informazioni (e.g. progetti di elettronica, videogiochi opensource, dimostrazioni di programmi liberi per la grafica o l'editing audio/video, un paio di postazioni Linux per metterci direttamente le mani...), o più semplicemente si può allestire un punto informativo presso cui distribuire materiali, gadgets, assistenza ed informazioni (opzione particolarmente azzeccata se vi trovate in un luogo di passaggio, come appunto un gazebo in piazza o un centro commerciale)
	</li>
	<li>
		pubblicate una <atring>pagina web</strong> dedicata all'evento, su cui fornire i dettagli indispensabili alla partecipazione: orari, indirizzo preciso della sede, programma... Prendete ispirazione ad esempio dai siti dei Linux Day passati per capire cosa può essere utile comunicare, e non dimenticate di esporre il <a href="<?php echo makeurl('/immagini/linuxday_fullcolor.svg') ?>">logo ufficiale del Linux Day</a>
	</li>
	<li>
		<strong>registrate</strong> il vostro evento su linuxday.it per essere ammessi all'indice nazionale delle iniziative: questo è probabilmente il canale preferenziale per entrare in contatto con il potenziale pubblico. Per farlo, occorre <a href="<?php echo makeurl('/user') ?>">registrare un account</a> e, dopo essersi autenticati, <a href="<?php echo makeurl('/registra') ?>">compilare questo form</a>
	</li>
	<li>
	fate <strong>promozione</strong>. La pubblicazione su linuxday.it non basta, e mai bisogna dare per scontato che le persone sappiano che ci sia il Linux Day: mandate un comunicato ai giornali locali, stampate qualche volantino da appendere nelle bacheche pubbliche, scrivete ai rappresentanti locali, insomma fatevi vedere!
	</li>
	<li>
		non fatevi cogliere alla sprovvista. Spesso capita qualcuno che porta il proprio computer per avere aiuto nell'installazione di Linux, o per risolvere qualche problematica tecnica specifica: portatevi qualche CD o qualche chiavetta USB avviabile per l'installazione. E preparate qualche cartello che indichi la vostra presenza, da appendere il giorno stesso intorno alla vostra zona onde rendervi più evidenti alle persone di passaggio
	</li>
	<li>
		<?php echo strtolower(conf('human_date')) ?>, <strong>divertitevi</strong> il più possibile!
	</li>
	<li>
		se siete soddisfatti della vostra opera, valutate la possibilità di costituirvi in <strong>LUG</strong> permanente. Tutti i dettagli sul come e perché, e le opportunità di assistenza, li potete trovare nel <a href="https://www.linux.it/manuale">"Manuale Operativo per la Community"</a> pubblicato da Italian Linux Society
	</li>
</ul>

<p>
	Qualcosa non è ancora chiaro? Serve maggiore supporto o chiarimenti? Non sapete dove pubblicare la pagina web per il vostro evento? Per qualsiasi informazione o richiesta potete scrivere a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
</p>

<p>
	E buon Linux Day!
</p>

<?php
lugfooter ();
