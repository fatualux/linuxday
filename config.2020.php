<?php

$internal_current_date_do_not_read = date('Y-m-d');

date_default_timezone_set("Europe/Rome");
$current_year = '2020';
$computer_date = '2020-10-24';
$shipping_date = 0;
$human_date = 'Sabato 24 Ottobre e Domenica 25 Ottobre 2020';
$administrators = ['bob@linux.it', 'ferdi.traversa@gmail.com'];

$is_virtual = true;
$is_physical = false;

$sessions = [
    'base' => (object) [
        'label' => 'Sessione Base',
        'desc' => 'Divulgazione, introduzione a concetti e strumenti base',
        'player' => 'https://garr.tv/s/5f8ec7c0fa966e708fd2be4c?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'dev' => (object) [
        'label' => 'Programmazione',
        'desc' => 'Framework, librerie, esperienze, risorse per sviluppatori',
        'player' => 'https://garr.tv/s/5f8ec812fa966eceb0d2be4d?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'desktop' => (object) [
        'label' => 'Applicazioni',
        'desc' => 'Talk generici o di approfondimento su applicazioni libere',
        'player' => 'https://garr.tv/s/5f8ec836fa966e1ec0d2be4e?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-24',
    ],
    'other' => (object) [
        'label' => 'Approfondimenti',
        'desc' => 'Interventi trasversali di approfondimento',
        'player' => 'https://garr.tv/s/5f903cf7ed71533554e3cc0c?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-25',
    ],
    'sys' => (object) [
        'label' => 'SysOps',
        'desc' => 'Applicazioni server-side, middleware, virtualizzazione, networking',
        'player' => 'https://garr.tv/s/5f903d27ed7153306ee3cc0d?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-25',
    ],
    'edu' => (object) [
        'label' => 'Scuola',
        'desc' => 'Strumenti, contenuti, metodologie per la didattica',
        'player' => 'https://garr.tv/s/5f903d4ced7153634be3cc0e?t=0',
        'live' => false || $internal_current_date_do_not_read == '2020-10-25',
    ],
];

$talks_date = '2020-07-31';
$human_talks_date = 'Venerdi 31 Luglio';

$sponsors = [
    'Software Workers' => (object) [
        'logo' => 'http://www.ils.org/assets/images/sponsor/softwareworkers.png',
        'link' => 'https://softwareworkers.it/',
    ],
    'Continuity' => (object) [
        'logo' => 'http://www.ils.org/assets/images/sponsor/continuity.png',
        'link' => 'https://continuity.space/',
    ],
    'Appleby' => (object) [
        'logo' => 'http://www.ils.org/assets/images/sponsor/appleby.png',
        'link' => 'http://www.applebyitalia.it/',
    ],
    'Linux Professional Institute' => (object) [
        'logo' => 'http://www.ils.org/assets/images/sponsor/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
    'Ergonet' => (object) [
        'logo' => 'http://www.ils.org/assets/images/sponsor/ergonet.png',
        'link' => 'https://www.ergonet.it',
    ],
];

$supporters = [
    'GARR' => (object) [
        'logo' => '/immagini/garr.png',
        'link' => 'https://garr.it/',
    ],
];

$patronages = [
    "Ministro per l'innovazione tecnologica e la digitalizzazione" => (object) [
        'logo' => '/immagini/mid.png',
        'link' => 'https://innovazione.gov.it/',
    ],
];

$theme = [];
