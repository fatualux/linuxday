# Linux Day (linuxday.it)

Questo è il codice sorgente del sito del [Linux Day](https://www.linuxday.it/), la principale conferenza annuale dedicata al software libero e all'open source in Italia.

https://www.linuxday.it/

## Contribuire

Per contribuire allo sviluppo di questo progetto, visita la pagina del progetto e clicca sul tasto Fork:

https://gitlab.com/ItalianLinuxSociety/linuxday

Poi scarica una copia sul tuo computer per iniziare a lavorarci:

```
git clone https://gitlab.com/ItalianLinuxSociety/linuxday
```

Dopo aver fatto vari `git commit` e vari `git push` puoi inviare una richiesta di unione al ramo principale:

https://gitlab.com/ItalianLinuxSociety/linuxday/-/merge_requests

Dubbi? Chiarimenti? Contattaci!

https://www.linux.it/contatti/

Grazie per le tue idee e i tuoi contributi!

## Licenza

Questo progetto ed ogni suo contributo è rilasciato dai contributori Linux Day attraverso la licenza GNU Affero General Public License.

Puoi fare:

* utilizzare questo software
* modificare questo software
* condividere questo software e le tue modifiche

Per qualsiasi scopo, anche per scopo commerciale, a patto di:

* rilasciare il tuo progetto e ogni tua modifica con le stesse libertà

Grazie mille per i tuoi contributi!
