<?php

$talks_date = '2023-09-30';
$human_talks_date = 'Venerdi 30 Settembre';

// alphabetical order
$sponsors = [
	'Continuity' => (object) [
		'logo' => 'https://www.ils.org/assets/images/sponsor/continuity.png',
		'link' => 'https://continuity.space/',
	],
	'Bosh Rexroth' => (object) [
		'logo' => 'https://www.ils.org/assets/images/sponsor/bosh_rexroth.png',
		'link' => 'https://www.boschrexroth.com/',
	],
	'Extraordy' => (object) [
		'logo' => 'https://www.ils.org/assets/images/sponsor/extraordy.png',
		'link' => 'https://www.extraordy.com/',
	],
	'Linux Professional Institute' => (object) [
		'logo' => 'https://www.ils.org/assets/images/sponsor/lpi.png',
		'link' => 'https://www.lpi.org/it/',
	],
];

$supporters = [
	'GARR' => (object) [
		'logo' => '/immagini/garr.png',
		'link' => 'https://garr.it/',
	],
];

$patronages = [];

$theme = [];
